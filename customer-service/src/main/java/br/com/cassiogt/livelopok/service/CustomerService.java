package br.com.cassiogt.livelopok.service;

import br.com.cassiogt.livelopok.client.CityClient;
import br.com.cassiogt.livelopok.domain.Customer;
import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.CustomerDTO;
import br.com.cassiogt.livelopok.dto.EditCustomerDTO;
import br.com.cassiogt.livelopok.exception.DataNotFoundException;
import br.com.cassiogt.livelopok.exception.ResourceAlreadyExistsException;
import br.com.cassiogt.livelopok.repository.CustomerRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CityClient cityClient;

    @Autowired
    public CustomerService(CustomerRepository customerRepository, CityClient cityClient) {
        this.customerRepository = customerRepository;
        this.cityClient = cityClient;
    }

    public CustomerDTO createCustumer(CustomerDTO customerDTO) {
        CityDTO city;
        try {
            city = cityClient.getCityById(customerDTO.getLivingCityId());
        } catch (Exception e) {
            city = null;
        }
        if (city == null) {
            throw new DataNotFoundException("Could not retrieve city with ID " + customerDTO.getLivingCityId());
        }

        if (this.customerRepository.existsByName(customerDTO.getName())) {
            throw new ResourceAlreadyExistsException(
                String.format("Customer with name %s already exists", customerDTO.getName()));
        }
        Customer customer = customerDTO.asEntity();
        customer.setLivingCity(city.getName());

        return CustomerDTO.fromEntity(this.customerRepository.save(customer));
    }

    public CustomerDTO findByName(String name) {
        return CustomerDTO.fromEntity(findCustomerByName(name));
    }

    public CustomerDTO findById(String id) {
        return CustomerDTO.fromEntity(findCustomerById(id));
    }

    public void delete(String id) {
        this.customerRepository.deleteById(id);
    }

    public CustomerDTO update(EditCustomerDTO customerDTO) {
        Customer customer = findCustomerById(customerDTO.getId());
        customer.setName(customerDTO.getName());
        return CustomerDTO.fromEntity(this.customerRepository.save(customer));
    }

    public List<CustomerDTO> findAll() {
        return this.customerRepository.findAll()
            .stream()
            .map(CustomerDTO::fromEntity)
            .collect(Collectors.toList());
    }

    private Customer findCustomerById(String id) {
        Optional<Customer> customerOptional = this.customerRepository.findById(id);
        if (!customerOptional.isPresent()) {
            throw new DataNotFoundException("No customer found with ID " + id);
        }
        return customerOptional.get();
    }

    private Customer findCustomerByName(String name) {
        Optional<Customer> customerOptional = this.customerRepository.findByName(name);
        if (!customerOptional.isPresent()) {
            throw new DataNotFoundException("No customer found with name " + name);
        }
        return customerOptional.get();
    }

}
