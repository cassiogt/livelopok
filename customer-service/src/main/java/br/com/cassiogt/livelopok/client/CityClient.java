package br.com.cassiogt.livelopok.client;

import br.com.cassiogt.livelopok.dto.CityDTO;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "cities", url = "${app.feign.location-apiv1}")
public interface CityClient {

    @RequestMapping(method = RequestMethod.GET, value = "/cities")
    List<CityDTO> getCities();

    @GetMapping(value = "/cities")
    List<CityDTO> getCitiesByName(@RequestParam String name);

    @GetMapping(value = "/cities/{id}")
    CityDTO getCityById(@PathVariable("id") Integer id);

    @PostMapping(value = "/cities", consumes = "application/json")
    CityDTO create(@RequestBody CityDTO city);
}
