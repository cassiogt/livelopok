package br.com.cassiogt.livelopok.domain;

public enum GenderEnum {
    FEMALE, MALE, UNDEFINED
}
