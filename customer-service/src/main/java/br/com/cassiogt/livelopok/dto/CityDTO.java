package br.com.cassiogt.livelopok.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CityDTO {

    private Integer id;

    private String name;

    private StateDTO state;

    private Integer stateId;

}
