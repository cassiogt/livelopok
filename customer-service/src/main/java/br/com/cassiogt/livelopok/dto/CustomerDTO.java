package br.com.cassiogt.livelopok.dto;

import br.com.cassiogt.livelopok.domain.Customer;
import br.com.cassiogt.livelopok.domain.GenderEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {

    @Schema(description = "The ID of the customer", example = "3423easd9uasf44")
    private String id;

    @Schema(description = "Customer's name", example = "Cássio", required = true)
    @NotBlank(message = "Customer's name should br informed")
    private String name;

    @Schema(description = "Customer's gender", example = "MALE, FEMALE, UNDEFINED", required = true)
    @NotNull(message = "Customer's genre should be informed")
    private GenderEnum gender;

    @Schema(description = "Customer's birthday", example = "2020-01-01", required = true)
    @NotNull(message = "Customer's birthday should be informed")
    private LocalDateTime birthday;

    @Schema(description = "ID of the city (third party service)", example = "1", required = true)
    @NotNull(message = "Customer's living city ID should be informed")
    private Integer livingCityId;

    private String livingCity;
    private String livingState;

    public Customer asEntity() {
        return Customer.builder()
            .id(this.getId())
            .name(this.getName())
            .gender(this.getGender())
            .birthday(this.getBirthday())
            .livingCity(this.getLivingCity())
            .livingState(this.getLivingState())
            .build();
    }

    public static CustomerDTO fromEntity(Customer customer) {
        return CustomerDTO.builder()
            .id(customer.getId())
            .name(customer.getName())
            .gender(customer.getGender())
            .birthday(customer.getBirthday())
            .livingCity(customer.getLivingCity())
            .livingState(customer.getLivingState())
            .build();
    }

}
