package br.com.cassiogt.livelopok.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class EditCustomerDTO {

    @Schema(description = "The Id of the customer", example = "3423easd9uasf44")
    private String id;

    @Schema(description = "Name of the city", example = "Santa Cruz do Sul", required = true)
    @NotBlank(message = "Customer's name should be informed")
    private String name;
}
