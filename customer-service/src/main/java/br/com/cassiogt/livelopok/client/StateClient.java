package br.com.cassiogt.livelopok.client;

import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.StateDTO;
import java.util.List;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "states", url = "${app.feign.location-apiv1}")
public interface StateClient {

    @GetMapping(value = "/states", consumes = "application/json")
    List<StateDTO> getStates();

    @GetMapping(value = "/states/{id}/cities", consumes = "application/json")
    List<CityDTO> getCitiesFromState(@PathVariable("id") Integer id);
}
