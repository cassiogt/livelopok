package br.com.cassiogt.livelopok.domain;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "custumers")
public class Customer {

    @Id
    private String id;

    @Indexed(unique = true)
    private String name;

    private GenderEnum gender;

    private LocalDateTime birthday;

    private String livingCity;

    private String livingState;

}
