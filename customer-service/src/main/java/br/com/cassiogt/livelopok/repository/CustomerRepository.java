package br.com.cassiogt.livelopok.repository;

import br.com.cassiogt.livelopok.domain.Customer;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {

    Optional<Customer> findByName(String name);

    boolean existsByName(String name);

}
