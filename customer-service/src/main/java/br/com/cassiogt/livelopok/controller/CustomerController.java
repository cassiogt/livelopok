package br.com.cassiogt.livelopok.controller;

import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.CustomerDTO;
import br.com.cassiogt.livelopok.dto.EditCustomerDTO;
import br.com.cassiogt.livelopok.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/customers")
@Tag(name = "customer", description = "The Customer API")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Operation(summary = "Creates a new Customer", tags = {"customer"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Customer resource created",
                    content = @Content(schema = @Schema(implementation = CustomerDTO.class))),
            @ApiResponse(responseCode = "400", description = "Invalid data sent"),
            @ApiResponse(responseCode = "404", description = "Customer not found")})
    @PostMapping
    public ResponseEntity<CustomerDTO> createCustomer(@Valid @RequestBody CustomerDTO customerDTO) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(this.customerService.createCustumer(customerDTO));
    }

    @Operation(summary = "Gets a list of customers", tags = {"customer"})
    @ApiResponse(responseCode = "200", description = "Customer list",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = CustomerDTO.class))))
    @GetMapping
    public ResponseEntity<List<CustomerDTO>> getAll() {
        return ResponseEntity.ok(this.customerService.findAll());
    }

    @Operation(summary = "Returns a customer searching by name", tags = {"customer"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Customer resource found",
                    content = @Content(schema = @Schema(implementation = CustomerDTO.class))),
            @ApiResponse(responseCode = "404", description = "Customer not found")})
    @GetMapping("/byname/{name}")
    public ResponseEntity<CustomerDTO> getByName(@NotBlank @PathVariable String name) {
        return ResponseEntity.ok(this.customerService.findByName(name));
    }

    @Operation(summary = "Returns a customer searching by ID", tags = {"customer"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Customer resource found",
                    content = @Content(schema = @Schema(implementation = CustomerDTO.class))),
            @ApiResponse(responseCode = "404", description = "Customer not found")})
    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> getById(@Valid @PathVariable String id) {
        return ResponseEntity.ok(this.customerService.findById(id));
    }

    @Operation(summary = "Deletes a customer by its ID", tags = {"customer"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Customer deleted"),
            @ApiResponse(responseCode = "404", description = "Customer not found")})
    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable String id) {
        this.customerService.delete(id);
    }

    @Operation(summary = "Updates a customer searching by ID", tags = {"customer"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Customer resource updated",
                    content = @Content(schema = @Schema(implementation = CustomerDTO.class))),
            @ApiResponse(responseCode = "404", description = "Customer not found")})
    @PutMapping("/{id}")
    public ResponseEntity<CustomerDTO> update(@PathVariable String id, @Valid @RequestBody EditCustomerDTO customerDTO) {
        customerDTO.setId(id);
        ;
        return ResponseEntity.ok(this.customerService.update(customerDTO));
    }

}
