package br.com.cassiogt.livelopok.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StateDTO {

    private Integer id;
    private String name;
}
