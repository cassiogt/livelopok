package br.com.cassiogt.livelopok;

import br.com.cassiogt.livelopok.client.CityClient;
import br.com.cassiogt.livelopok.domain.GenderEnum;
import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.CustomerDTO;
import br.com.cassiogt.livelopok.dto.StateDTO;
import br.com.cassiogt.livelopok.exception.DataNotFoundException;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@DirtiesContext
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureDataMongo
public class CustomerIntegrationTests {

    private static final String CUSTOMERS_URL = "/api/v1/customers";

    @MockBean
    private CityClient cityClient;

    @Autowired
    private TestRestTemplate restTemplate;

    CustomerDTO defaultCustomer = CustomerDTO.builder()
            .name("Test")
            .birthday(LocalDateTime.now())
            .gender(GenderEnum.UNDEFINED)
            .build();

    @BeforeEach
    void beforeEach() {
        when(cityClient.getCityById(1))
                .thenReturn(new CityDTO(1, "Test", new StateDTO(1, "RS"), 1));
        when(cityClient.getCityById(2)).thenThrow(new DataNotFoundException("TEST"));
    }

    @DisplayName("On call API to create new customer with City ID 1, return CREATED")
    @Test
    public void createCustomerOk() {

        defaultCustomer.setName("Name OK");
        defaultCustomer.setLivingCityId(1);
        ResponseEntity<CustomerDTO> responseEntity = restTemplate.postForEntity(CUSTOMERS_URL, defaultCustomer,
                CustomerDTO.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode(), "HTTP response should be 200");
        assertNotNull(responseEntity.getBody().getId(), "New customer should have an ID");
    }

    @DisplayName("On call API to create new customer with City ID 2, return NOT_FOUND")
    @Test
    public void createCustomerError() {

        defaultCustomer.setName("Name Error");
        defaultCustomer.setLivingCityId(2);
        ResponseEntity<CustomerDTO> responseEntity = restTemplate.postForEntity(CUSTOMERS_URL, defaultCustomer,
                CustomerDTO.class);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode(), "HTTP response should be 404");
    }

    @DisplayName("On call API to create new customer with already used name, return CONFLICT")
    @Test
    public void createCustomerConflict() {

        defaultCustomer.setName("Name Conflict");
        defaultCustomer.setLivingCityId(1);
        ResponseEntity<CustomerDTO> responseEntity = restTemplate.postForEntity(CUSTOMERS_URL, defaultCustomer,
                CustomerDTO.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode(), "HTTP response should be 200");

        ResponseEntity<CustomerDTO> responseEntity2 = restTemplate.postForEntity(CUSTOMERS_URL, defaultCustomer,
                CustomerDTO.class);
        assertEquals(HttpStatus.CONFLICT, responseEntity2.getStatusCode(), "HTTP response should be 200");
    }

    @DisplayName("On call API to update customer, return OK")
    @Test
    public void createCustomerUpdate() {

        defaultCustomer.setName("Name Update");
        defaultCustomer.setLivingCityId(1);
        ResponseEntity<CustomerDTO> responseEntity =
                restTemplate.postForEntity(CUSTOMERS_URL, defaultCustomer, CustomerDTO.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode(), "HTTP response should be 200");

        defaultCustomer.setName("Name Updated");
        restTemplate.put(CUSTOMERS_URL + "/" + responseEntity.getBody().getId(), defaultCustomer);

        ResponseEntity<CustomerDTO> responseEntity2 =
                restTemplate.getForEntity(CUSTOMERS_URL + "/byname/" + defaultCustomer.getName(),
                        CustomerDTO.class);
        assertEquals(HttpStatus.OK, responseEntity2.getStatusCode(), "HTTP response should be 200");
    }

    @DisplayName("On call API to delete customer, return OK")
    @Test
    public void createCustomerDelete() {

        defaultCustomer.setName("Name Delete");
        defaultCustomer.setLivingCityId(1);
        ResponseEntity<CustomerDTO> responseEntity = restTemplate.postForEntity(CUSTOMERS_URL, defaultCustomer,
                CustomerDTO.class);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode(), "HTTP response should be 200");

        restTemplate.delete(CUSTOMERS_URL + "/" + defaultCustomer.getId());

        ResponseEntity<CustomerDTO> responseEntity2 =
                restTemplate.getForEntity(CUSTOMERS_URL + "/" + defaultCustomer.getId(),
                        CustomerDTO.class);

        assertEquals(HttpStatus.NOT_FOUND, responseEntity2.getStatusCode(), "HTTP response should be 200");
    }
}