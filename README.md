# livelopok
Livelo Prove Od Knowledge Project

## About
This project aims to demonstrate knowledge in Microservices acquired during past experience.
There are three projects, two services and an API gateway. Also, each project uses a different database.

### location-service
- Service that returns and saves data about States and Cities;
- Uses PostgreSQL as database and H2 database for testing;
- Uses cache on states and cities request, since they are not updated frequently.

### customer-service
- Service that returns and saves data about Customers;
- Uses MongoDB as database and Embedded MongoDB for testing;
- Uses Feign client to request data from other APIs.

### gateway
- Uses spring cloud non blocking gateway for API routing;

### service-discovery, load-balancer, config-service, security
- Since this project uses only two services, I opted for not implementing these resources because there would be a
 higher resource cost, and may be considered over engineering.

## Prerequisites
To compile this software maven should be installed.
To run this software, a JRE and a PostgresSQL and MongoDB are needed.
You can also run this using docker-compose

## Building
To compile this projects run:
```shell script
mvn clean compile
```
To test this projects run:
```shell script
mvn test
```
To package this projects run:
```shell script
mvn package
```

## Running
To run, call docker-compose inside project root directory:
```shell script
docker-compose up
```

## API Usage
Below are the API endpoints to be called, replace the <DATA> with the apropriate value.

- Get a list of states:
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/states
```

- Get a state by ID:
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/states/<STATE_ID>
```

- Get a list of cities from a state by state ID:
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/states/<STATE_ID>/cities
```

- Get a list of cities, can be filtered using query parameters 'name' and 'state', where 'name' is the name of the city, and state can be the complete name of the state, an abbreviation or the state's ID.
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/cities
```

- Get a city by ID;
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/cities/<CITY_ID>
```

- Create a new city;
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{ "name": "<CITY_NAME>", "stateId": <STATE_ID> }' \
  http://localhost:8080/api/v1/cities
```

- Get a list of customers:
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/customers
```

- Get a customer by ID:
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/customers/<CUSTOMER_ID>
```

- Get a customer by name:
```shell script
curl --header "Content-Type: application/json" \
  --request GET \
  http://localhost:8080/api/v1/customers/byname/<CUSTOMER_NAME>
```

- Create a new customer:
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{ "name": "<CUSTOMER_NAME>", "genre": "<MALE,FEMALE,UNDEFINED>", "birthday": "<ISO_DATE>",  "livingCityId": <CITY_ID>}' \
  http://localhost:8080/api/v1/customers
```

- Update customer's name:
```shell script
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{ "name": "<CUSTOMER_NAME>"}' \
  http://localhost:8080/api/v1/customers/<CUSTOMER>ID>
```

- Delete a customer:
```shell script
curl --header "Content-Type: application/json" \
  --request DELETE \
  http://localhost:8080/api/v1/customers/<CUSTOMER>ID>
```
