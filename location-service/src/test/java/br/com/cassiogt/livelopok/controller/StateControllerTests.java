package br.com.cassiogt.livelopok.controller;

import br.com.cassiogt.livelopok.dto.StateDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StateControllerTests {

    private static final String STATES_URL = "/api/v1/states";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void retrieveStates() {
        ResponseEntity<StateDTO[]> responseEntity = restTemplate.getForEntity(STATES_URL, StateDTO[].class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode(), "HTTP response should be 200");
        assertTrue(responseEntity.getBody().length > 0, "States list should not be empty");
    }

}
