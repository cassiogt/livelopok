package br.com.cassiogt.livelopok.controller;

import br.com.cassiogt.livelopok.dto.CityDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityControllerTests {

    private static final String CITIES_URL = "/api/v1/cities";

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void retrieveCities_shouldReturnNotFound() {
        ResponseEntity<CityDTO[]> responseEntity = restTemplate.getForEntity(CITIES_URL, CityDTO[].class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode(), "HTTP response should be 404");
        assertTrue(responseEntity.getBody().length == 0, "Cities list should be empty");
    }

}
