package br.com.cassiogt.livelopok.dto;

import br.com.cassiogt.livelopok.entity.City;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CityDTO {

    @Setter(AccessLevel.NONE)
    @Schema(description = "ID of the city", example = "1")
    private Integer id;

    @Schema(description = "Name of the city", example = "Santa Cruz do Sul", required = true)
    @NotBlank(message = "City's name shouldn't be null")
    @Size(max = 100, message = "City's name should not be longer than 100 characters")
    private String name;

    @Schema(description = "State object", example = "Santa Cruz do Sul", required = true)
    private StateDTO state;

    @Schema(description = "State ID", example = "4", required = true)
    @NotNull(message = "State ID shouldn't be null")
    private Integer stateId;

    /**
     * Converts {@link CityDTO} to a {link City} object.
     *
     * @return a {@link City} representation of this object.
     */
    public City asEntity() {
        return City.builder()
            .id(this.getId())
            .name(this.getName())
            .build();
    }

    /**
     * Converts {@link City} to a {link CityDTO} object.
     *
     * @return a {@link CityDTO} representation of
     * the {@link City} object passed by parameter.
     */
    public static CityDTO fromEntity(City city) {
        CityDTO cityDTO = CityDTO.builder()
            .id(city.getId())
            .name(city.getName())
            .build();
        if (city.getState() != null) {
            cityDTO.setState(StateDTO.fromEntity(city.getState()));
        }
        return cityDTO;
    }

}
