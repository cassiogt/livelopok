package br.com.cassiogt.livelopok.service;

import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.StateDTO;
import br.com.cassiogt.livelopok.entity.State;
import br.com.cassiogt.livelopok.exception.DataNotFoundException;
import br.com.cassiogt.livelopok.repository.StateRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implements business logic for states.
 */
@Service
public class StateService {

    private final StateRepository repository;
    private final CityService cityService;

    @Autowired
    public StateService(StateRepository repository, @Lazy CityService cityService) {
        this.repository = repository;
        this.cityService = cityService;
    }

    /**
     * Returns a list of states sorted by name,
     * or throws {@link DataNotFoundException} if no data were found.
     *
     * @return a {@link List} of {@link StateDTO}.
     */
    @Cacheable("states")
    @Transactional(readOnly = true)
    public List<StateDTO> findAllSortedByName() {
        List<State> stateList = this.repository.findAll(Sort.by("name"));
        return stateList
            .stream()
            .map(StateDTO::fromEntity)
            .collect(Collectors.toList());
    }

    /**
     * Returns a {@link StateDTO} with ID passed by parameter.
     * or throws {@link DataNotFoundException} if no data were found.
     *
     * @return an {@link StateDTO}.
     */
    @Transactional(readOnly = true)
    public StateDTO findById(Integer id) {
        Optional<State> stateOptional = this.repository.findById(id);
        if (!stateOptional.isPresent()) {
            throw new DataNotFoundException("Could not find State with ID " + id);
        }

        return StateDTO.fromEntity(stateOptional.get());
    }

    public List<CityDTO> findCitiesByStateId(Integer id) {

        if (!this.repository.existsById(id)) {
            throw new DataNotFoundException("Could not find State with ID " + id);
        }

        return this.cityService.findByStateID(id);
    }
}
