package br.com.cassiogt.livelopok.repository;

import br.com.cassiogt.livelopok.entity.City;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends JpaRepository<City, Integer>, JpaSpecificationExecutor<City> {

    @Query("select count(c) from City c where c.state.id = :stateId and c.name = :name")
    Long countAllByNameAndStateId(@Param("name") String name, @Param("stateId") Integer stateId);

    @Query("select c from City c where c.state.id = :stateId")
    List<City> findAllByStateId(@Param("stateId") Integer stateId);

}
