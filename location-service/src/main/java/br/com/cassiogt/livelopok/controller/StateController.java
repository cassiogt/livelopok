package br.com.cassiogt.livelopok.controller;

import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.StateDTO;
import br.com.cassiogt.livelopok.service.StateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * API endpoint for state resource.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/states")
@Tag(name = "state", description = "The State API")
public class StateController {

    private final StateService stateService;

    @Autowired
    public StateController(StateService stateService) {
        this.stateService = stateService;
    }

    @Operation(summary = "Find States ordered by name", tags = {"state"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = StateDTO.class))))})
    @GetMapping
    public ResponseEntity<List<StateDTO>> fetchAll() {
        return ResponseEntity.ok(stateService.findAllSortedByName());
    }

    @Operation(summary = "Find a State by ID", tags = {"state"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(schema = @Schema(implementation = StateDTO.class))),
        @ApiResponse(responseCode = "404", description = "State not found")})
    @GetMapping("/{id}")
    public ResponseEntity<StateDTO> fetchOne(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(stateService.findById(id));
    }

    @Operation(summary = "Get cities from state", tags = {"state"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(schema = @Schema(implementation = StateDTO.class))),
        @ApiResponse(responseCode = "404", description = "State not found")})
    @GetMapping("/{id}/cities")
    public ResponseEntity<List<CityDTO>> fetchCitiesFromState(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(stateService.findCitiesByStateId(id));
    }
}
