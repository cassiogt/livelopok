package br.com.cassiogt.livelopok.service;

import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.dto.StateDTO;
import br.com.cassiogt.livelopok.entity.City;
import br.com.cassiogt.livelopok.exception.DataNotFoundException;
import br.com.cassiogt.livelopok.exception.ResourceAlreadyExistsException;
import br.com.cassiogt.livelopok.repository.CityRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implements business logic for cities.
 */
@Slf4j
@Service
public class CityService {

    private final CityRepository repository;
    private final StateService stateService;

    @Autowired
    public CityService(CityRepository repository,
                       StateService stateService) {
        this.repository = repository;
        this.stateService = stateService;
    }

    /**
     * Creates a new City or throws RuntimeException if an error occurs;
     *
     * @param cityDTO the City information to be saved.
     * @return a {@link CityDTO} instance with ID set.
     */
    @CacheEvict(value = "cities", allEntries = true)
    @Transactional
    public CityDTO create(CityDTO cityDTO) {

        cityDTO.setName(cityDTO.getName().toUpperCase());
        StateDTO stateDTO = this.stateService.findById(cityDTO.getStateId());

        if (this.repository.countAllByNameAndStateId(cityDTO.getName(), stateDTO.getId()) > 0) {
            throw new ResourceAlreadyExistsException(String.format("City for name %s and state %s already record.",
                cityDTO.getName(), stateDTO.getName()));
        }

        return CityDTO.fromEntity(
            this.repository.save(City.builder()
                .name(cityDTO.getName())
                .state(stateDTO.asEntity())
                .build()));
    }

    /**
     * Find all cities searching by Specification passed by parameter.
     *
     * @param search a {@link City} {@link Specification}.
     * @return a list of cities.
     */
    @Cacheable(value = "cities")
    @Transactional(readOnly = true)
    public List<CityDTO> findAll(Specification<City> search) {

        List<City> cityList = this.repository.findAll(search);
        return cityList
            .stream()
            .map(CityDTO::fromEntity)
            .collect(Collectors.toList());
    }

    /**
     * Returns a {@link CityDTO} with ID passed by parameter.
     * or throws {@link DataNotFoundException} if no data were found.
     *
     * @return an {@link CityDTO}.
     */
    @Transactional(readOnly = true)
    public CityDTO findById(Integer id) {
        Optional<City> cityOptional = this.repository.findById(id);
        if (!cityOptional.isPresent()) {
            throw new DataNotFoundException("Could not find City with ID " + id);
        }

        return CityDTO.fromEntity(cityOptional.get());
    }

    /**
     * Returns a list of {@link CityDTO} from state passed by parameter.
     *
     * @param stateId the ID of te state to be searched.
     * @return an {@link CityDTO} list.
     */
    @Transactional(readOnly = true)
    public List<CityDTO> findByStateID(Integer stateId) {
        return this.repository.findAllByStateId(stateId).stream()
            .map(CityDTO::fromEntity)
            .collect(Collectors.toList());
    }
}
