package br.com.cassiogt.livelopok.dto;

import br.com.cassiogt.livelopok.entity.State;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A data transfer object for State.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StateDTO {

    @Setter(AccessLevel.NONE)
    @Schema(description = "ID of the state", example = "1")
    private Integer id;

    @Schema(description = "Name of the state", example = "Rio Grande do Sul", required = true)
    @NotBlank(message = "State name shouldn't be null")
    @Size(max = 100, message = "State name should not be longer than 100 characters")
    private String name;

    @Schema(description = "Code of the state, with two letters", example = "RS", required = true)
    @NotBlank(message = "State code shouldn't be null")
    @Size(min = 2, max = 2, message = "State code should only have two characters")
    private String alpha2Code;

    /**
     * Converts {@link StateDTO} to a {link State} object.
     *
     * @return a {@link State} representation of this object.
     */
    public State asEntity() {
        return State.builder()
            .id(this.getId())
            .name(this.getName())
            .alpha2Code(this.getAlpha2Code())
            .build();
    }

    /**
     * Converts {@link State} to a {link StateDTO} object.
     *
     * @return a {@link StateDTO} representation of
     * the {@link State} object passed by parameter.
     */
    public static StateDTO fromEntity(State state) {
        return StateDTO.builder()
            .id(state.getId())
            .name(state.getName())
            .alpha2Code(state.getAlpha2Code())
            .build();
    }

}
