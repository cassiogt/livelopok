package br.com.cassiogt.livelopok.repository;

import br.com.cassiogt.livelopok.entity.State;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends JpaRepository<State, Integer> {

}
