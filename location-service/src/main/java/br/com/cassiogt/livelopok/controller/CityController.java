package br.com.cassiogt.livelopok.controller;

import br.com.cassiogt.livelopok.dto.CityDTO;
import br.com.cassiogt.livelopok.entity.City;
import br.com.cassiogt.livelopok.service.CityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Conjunction;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * API endpoint for city resource.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1/cities")
@Tag(name = "city", description = "The City API")
public class CityController {

    private CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @Operation(summary = "Creates a new City", tags = {"city"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "City resource created",
            content = @Content(schema = @Schema(implementation = CityDTO.class))),
        @ApiResponse(responseCode = "400", description = "Incorrect data sent")})
    @PostMapping
    public ResponseEntity<CityDTO> createCity(@Valid @RequestBody CityDTO cityDTO) {
        return ResponseEntity.status(HttpStatus.CREATED).body(cityService.create(cityDTO));
    }

    @Operation(summary = "Fetches a list of cities",
        description = "Fetches a list of cities, can be used with filters by name a/or state", tags = {"city"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Operation successful",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = CityDTO.class)))),
        @ApiResponse(responseCode = "400", description = "Incorrect data sent")})
    @GetMapping()
    public ResponseEntity<List<CityDTO>> fetchAll(
        @Conjunction({
            @Or(@Spec(path = "name", params = "name", spec = LikeIgnoreCase.class)),
            @Or({
                @Spec(path = "state.name", params = "state", spec = LikeIgnoreCase.class),
                @Spec(path = "state.alpha2Code", params = "state", spec = LikeIgnoreCase.class),
                @Spec(path = "state.id", params = "state", spec = Equal.class)})})
            Specification<City> citySpec) {
        return ResponseEntity.ok(cityService.findAll(citySpec));
    }

    @Operation(summary = "Find a City by ID", tags = {"city"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation",
            content = @Content(schema = @Schema(implementation = CityDTO.class))),
        @ApiResponse(responseCode = "404", description = "State not found")})
    @GetMapping("/{id}")
    public ResponseEntity<CityDTO> fetchOne(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(cityService.findById(id));
    }
}
