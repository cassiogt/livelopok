-- STATE --
CREATE TABLE IF NOT EXISTS public.state (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100),
    alpha2_code VARCHAR(2) NULL
);

COMMENT ON TABLE public.state IS 'States table';
COMMENT ON COLUMN public.state.id IS 'Tuple identifier';
COMMENT ON COLUMN public.state.name IS 'Name of the state';
COMMENT ON COLUMN public.state.alpha2_code IS 'Code of the state with two characters';

INSERT INTO state (name, alpha2_code)
    VALUES
    ('DISTRITO FEDERAL', 'DF'),
    ('ACRE', 'AC'),
    ('ALAGOAS', 'AL'),
    ('AMAPÁ', 'AP'),
    ('AMAZONAS', 'AM'),
    ('BAHIA', 'BA'),
    ('CEARÁ', 'CE'),
    ('ESPÍRITO SANTO', 'ES'),
    ('GOIÁS', 'GO'),
    ('MARANHÃO', 'MA'),
    ('MATO GROSSO', 'MT'),
    ('MATO GROSSO DO SUL', 'MS'),
    ('MINAS GERAIS', 'MG'),
    ('PARÁ', 'PA'),
    ('PARAÍBA', 'PB'),
    ('PARANÁ', 'PR'),
    ('PERNAMBUCO', 'PE'),
    ('PIAUÍ', 'PI'),
    ('RIO DE JANEIRO', 'RJ'),
    ('RIO GRANDE DO NORTE', 'RN'),
    ('RIO GRANDE DO SUL', 'RS'),
    ('RONDÔNIA', 'RO'),
    ('RORAIMA', 'RR'),
    ('SANTA CATARINA', 'SC'),
    ('SÃO PAULO', 'SP'),
    ('SERGIPE', 'SE'),
    ('TOCANTINS', 'TO');

-- CITY --
CREATE TABLE IF NOT EXISTS public.city (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    state_id INTEGER NOT NULL REFERENCES public.state (id),
    CONSTRAINT city_name_state_id_uq UNIQUE (name, state_id)
);

COMMENT ON TABLE public.city IS 'Cities table';
COMMENT ON COLUMN public.city.id IS 'Tuple identifier';
COMMENT ON COLUMN public.city.name IS 'Name of the city';
COMMENT ON COLUMN public.city.state_id IS 'Foreign key for the state';


